// Function showing ingredients for each pizza on hover
$(function(){
  $('label').hover(
      function(){
        $(this).children('span').show();
      },
      function(){
        $(this).children('span').hide();
      }
  )
})

//Function showing the right amount of slices for depending on the chosen amount
$(function(){
  $('div.nb-parts').children("input").change(
    function(){
      //get input value
      let slicesNumber = $(".nb-parts").children("input").val();

      if(slicesNumber<=6){
        //change the number of slices on one pizza
        $("div.nb-parts").children("span.pizza-pict").addClass(`pizza-${slicesNumber}`);
      }
      else{
        //calculate conditions
        let pizzaNumber = slicesNumber/6;
        let roundedPizzaNumber = Math.ceil(pizzaNumber);
        let uniqueSlices = slicesNumber % 6;

        //remove the start pizza to have the right number
        $("div.nb-parts").children("span.pizza-pict").remove();

        for(let i=0; i<(roundedPizzaNumber-1); i++){
          //show full pizzas
          let element = $('<span class="pizza-6 pizza-pict"></span>');
          element.clone().appendTo("div.nb-parts");
        }

        //show the remaining slices
        let element = $(`<span class="pizza-${uniqueSlices} pizza-pict"></span>`);
        element.clone().appendTo("div.nb-parts");
      }
    }
  )
});

//Function to show the form
$(function(){
  $(".next-step").click(
    function(){
      $(".infos-client").removeClass("no-display");
      $(".next-step").remove();
    }
  )
});

//Function to add a new Adress
$(function(){
  $(".btn-default").click(
    function(){
      $(this).parent().prepend('<input style="display:block" type="text"/>');
    }
  )
})

//Function confirmation message
$(function(){
  $(".done").click(
    function(){
      let prenom = $(".infos-client div.type").first().children("input").val();

      $("body").empty();

      alert(`Merci ${prenom} ! Votre commande sera livrée dans 15 minutes`)

    }
  )
})

//Function showing total to pay
$(document).on('change', 'input', function(){
  let totalPrice;

  //Get the price for the type of pizza and the number of slices
  let slicesNumber = $("div.nb-parts").children("input").val();
  let pricePerSlices = slicesNumber/6;
  let pizzaType = $("input[name=type]:checked").data("price");

  let basicPizzaPrice = (pizzaType * pricePerSlices).toFixed(2);

  //Get the price for the type of dough used
  let patePrice = $("input[name=pate]:checked").data("price");

  //Get the price for the extras
  let extraPrice=0;

  $('input[name=extra]:checked').each(function() {
     extraPrice += parseInt($(this).data('price'),10);
  });

  //Calculate and show the total price
  totalPrice = parseFloat(basicPizzaPrice) + parseFloat(patePrice) + extraPrice ;

  $(".stick-right p").html(`${totalPrice} €`);
})